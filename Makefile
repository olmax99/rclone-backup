.ONESHELL:
.SHELL := /usr/bin/bash
.PHONY: bkup-target bkup-run _set_target_timestamp _set-env

include .makerc
export (shell sed -E -e "s/^([^#])/export \1/" -e "s/=/='/" -e "s/(=.*)$/\1'/" .makerc)

TIMESTAMP="$(shell cat .set_backup_time.txt)"
REPLICA_REGION=eu-west-1
CURRENT_FOLDER=$(shell pwd)
BOLD=$(shell tput bold)
RED=$(shell tput setaf 1)
GREEN=$(shell tput setaf 2)
YELLOW=$(shell tput setaf 3)
RESET=$(shell tput sgr0)

target: help
	$(info ${HELP_MESSAGE})
	@exit 0

help:
	@echo "$(YELLOW)$(BOLD)[INFO] ...::: WELCOME :::...$(RESET)"
	head -n 5 ./LICENCE.md && tail -n 2 ./LICENCE.md && printf "\n\n"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

bkup-target: _set-env ## Create an s3 Helm repo Bucket - see Readme.md for further instructions
	aws cloudformation --profile ${AWS_PROFILE} --region ${AWS_REGION} create-stack --stack-name ${AWS_PROFILE}-${PROJECT_SLUG}-backup \
	--template-body file://cloudformation/cfn.backup.target.yml \
	--parameters ParameterKey="BackupTargetName",ParameterValue="${PROJECT_NAME}-${PROJECT_SLUG}-backup-${AWS_REGION}" \
	ParameterKey="ReplicationBucketName",ParameterValue="${PROJECT_NAME}-${PROJECT_SLUG}-backup-replica-${REPLICA_REGION}" \
	ParameterKey="RepRegion",ParameterValue="${REPLICA_REGION}" \
	--capabilities CAPABILITY_IAM

bkup-run: _set-env  ## Update, .
	@echo "$(YELLOW)$(BOLD)[INFO] Sync local directories with backup target.$(RESET)"
	@scripts/sync-repo.sh ~/${LOCAL_FOLDER} ${TARGET_BUCKET}/${TARGET_KEY}/${TIMESTAMP} ${AWS_PROFILE}

#############
#  Helpers  #
#############

_set_target_timestamp: ## renew every 10 days
	$(info $(BOLD)[+] Reset backup timestamp ..$(RESET))
	@rm -f .set_backup_time.txt
	@echo $(shell date "+%Y%m%d_%H%M%S") > .set_backup_time.txt	

_set-env: ## Confirm that all required Environment Variables have been set.
	@if [ -z $(AWS_PROFILE) ]; then \
		echo "$(BOLD)$(RED)AWS_PROFILE was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(AWS_REGION) ]; then \
		echo "$(BOLD)$(RED)AWS_REGION was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(TARGET_BUCKET) ]; then \
		echo "$(BOLD)$(RED)TARGET_BUCKET was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(TARGET_KEY) ]; then \
		echo "$(BOLD)$(RED)TARGET_BUCKET was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(PROJECT_SLUG) ]; then \
		echo "$(BOLD)$(RED)PROJECT_SLUG was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(PROJECT_NAME) ]; then \
		echo "$(BOLD)$(RED)PROJECT_NAME was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(LOCAL_FOLDER) ]; then \
		echo "$(BOLD)$(RED)LOCAL_FOLDER was not set.$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ ! -z $${ERROR} ] && [ $${ERROR} -eq 1 ]; then \
		echo "$(BOLD)Example usage: \`make bkup-run\`$(RESET)"; \
		exit 1; \
	 fi


define HELP_MESSAGE

	Environment variables to be aware of or to hardcode depending on your use case:

	AWS_REGION
		Default: not_defined
		Info: Environment variable to declare the default AWS Region (aws configure)

	AWS_PROFILE
		Default: not_defined
		Info: Environment variable to declare which configured aws-profile to use (aws configure)

	TARGET_BUCKET
		Default: not_defined
		Info: Environment variable to declare company and/or project name

	TARGET_KEY
		Default: not_defined
		Info: Environment variable to declare company and/or project name

	PROJECT_SLUG
		Default: not_defined
		Info: Think of a very short form of the project name with only lower-case letters and no special chars

	PROJECT_NAME
		Default: not_defined
		Info: Think of a project name with only lower-case letters and no special chars

	LOCAL_FOLDER
		Default: not_defined
		Info: Project version mainly for running multiple dev environments in parallel

	$(GREEN)Common usage:$(RESET)

	$(BOLD)...::: Set new timestamp key :::...$(RESET)
	$(GREEN)~$$$(RESET) make _set_target_timestamp

	$(BOLD)...::: Create a target bucket in cloudformation :::...$(RESET)
	$(GREEN)~$$$(RESET) make bkup-target

	$(BOLD)...::: Sync local folder to target bucket :::...$(RESET)
	$(GREEN)~$$$(RESET) make bkup-run

endef
