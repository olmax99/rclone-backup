#!/usr/bin/env bash
set -euxo pipefail
main() {
  cd /usr/local/src/gitlab.com/olmax/rclone-backup
  echo "y" | make $1 
}

main "${1:-bkup-run}"
